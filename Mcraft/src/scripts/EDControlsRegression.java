package scripts;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import com.relevantcodes.extentreports.LogStatus;
import commonFunctions.FrameworkFunctions;
import commonFunctions.SeleniumFunctions;
import driver.Driver;
import objectRepository.*;

public class EDControlsRegression extends Driver{
	
	// Launching URL
	public static void openApplicationURL() throws Exception
	{
		try{
			
				testCaseName="OpenApplicationURL";
				log=report.startTest(testCaseName);
				driver.get(PRODUCTION_ENVIRONMENT);
				log.log(LogStatus.PASS, "Application url loaded Successfully");	
					
		}catch(Exception e){
			FrameworkFunctions.screenShot("Error"+testCaseName);
			log.log(LogStatus.FAIL, e.getMessage());
		}
		}
	
	//	Register for user login
	
	public static void registerNewUser (String[] data,String[] assertion) throws Exception {
		
		try {
			
				testCaseName="registerNewUser";
				log=report.startTest(testCaseName, "This Method will perform funtion like Sign up to Ed-control website by entering the right credentials and other data");
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.UserName, "User Name");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.SignUp, "sign Up");
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.SignUpFirstName, "Sign up First Name");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.SignUpNewEmailID, data[0], "SignUp New EmailID");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.SignUpNewPassWord, data[1], "SignUp New PassWord");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.SignUpConfirmPassWord, data[2], "Sign up Confirm Password");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.SignUpFirstName, data[3], "SignUp First Name");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.SignUplastName	, data[4], "Sign Up Last Name");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.SignUpCompanyName	, data[5], "Sign Up Company Name");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.SignUpContactNumber, data[6], "Sign Up Contact Number");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.SignUpAddress, data[7], "Sign Up Address");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.SignUpPostalCode, data[8], "Sign Up Postal Code");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.SignUpCity, data[9], "Sign Up City");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.SignUpCountry, data[10], "Sign Up Country");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.NewRegisterSignUp, "Sign Up Button");
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.DoneButton, "Done Button");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.DoneButton, "Done Button");
				
		} catch (Exception e) {
			FrameworkFunctions.screenShot("Error"+testCaseName);
			log.log(LogStatus.FAIL, e.getMessage());
			
		}
	}
										
	
	// Normal Login 
	public static void login(String[] data,String[] assertion)
	
	{
		try{
			
			
				testCaseName="Login";
				log=report.startTest(testCaseName, "This Method will perform funtion like Log In to Ed-control website by entering the right credentials");
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.UserName, "User Name");			
				SeleniumFunctions.EnterText(EDControlsRegressionPage.UserName , data[0], "user Name");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.Password, data[1], "Password");
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.LogIn, "Login button");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.LogIn, "Login button");
			
		
			
				}catch(Exception e){
			
					System.out.println(e);
			try {
				
				FrameworkFunctions.screenShot("Error"+testCaseName);
		} catch (HeadlessException | IOException | AWTException e1) {
				e1.printStackTrace();
			}
			}
			}
	
	// Creating New Project 
	public static void CreateNewProject(String[] data,String[] assertion) throws Exception
	{
		try{
			
				testCaseName="CreateNewProject";
				log=report.startTest(testCaseName, "This Method will perform funtion like creating new project");
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.Newproject, "New Project");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.Newproject, "New Project");
				SeleniumFunctions.TransferToControlBrowser();
				SeleniumFunctions.SelectItemFromListboxByValue(EDControlsRegressionPage.SelectTheContract, data[0], "DropDown Item Selected");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.ProjectName, data[1], "Project Name ");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.Location, data[2], "Location");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.Accountable, data[3], "Accounatable");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.CreateButton, "Create");
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.OKbuttonInPopup, "OK Button");
				driver.navigate().refresh();
	
		}
		catch (Exception e) {
			log.log(LogStatus.FAIL, e.getMessage());
			FrameworkFunctions.screenShot("Error"+testCaseName);
		}
		}
	
	
	//Select project from existing project list
	
	public static void SelectExistingProjectFromList() throws HeadlessException, IOException, AWTException {
		
		try {
				testCaseName="SelectExistingProjectFromList";
				log=report.startTest(testCaseName, "This method will perform clicking on particular Existing Project in the list");
				//SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.Newproject,"New Project");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.ExistingProject	, "Existing Project");
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.Drawing, "Drawing");
			
		} catch (Exception e) {
			log.log(LogStatus.FAIL, e.getMessage());
			FrameworkFunctions.screenShot("Error"+testCaseName);
		}
		}
	
	
	//search for project in search box
	
	public static void SearchForExistingproject(String[] data,String[] assertion) throws HeadlessException, IOException, AWTException {
		try
		{
			testCaseName="SearchForExistingproject";
			log=report.startTest(testCaseName, "This method will Search for particular Existing Project by using search box");
			SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.Newproject, "New Project");
			driver.findElement(By.id("search-proj")).sendKeys(data[0]);
			FrameworkFunctions.screenShot(testCaseName);
			Thread.sleep(2000);
			driver.findElement(By.id("search-proj")).sendKeys(Keys.ENTER);
			FrameworkFunctions.screenShot(testCaseName);
			SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.SearchedProject, "Searched Project Name");
			Thread.sleep(2000);
			SeleniumFunctions.ClickButton(EDControlsRegressionPage.searchedTextClear, "Searched Project Text");
			
		}
		catch (Exception e) {
			log.log(LogStatus.FAIL, e.getMessage());
			FrameworkFunctions.screenShot("Error"+testCaseName);
		}
		}
	
	//method to navigate to general Settings
	public static void GeneralSettingsNavigation() throws HeadlessException, IOException, AWTException {
		try {
		
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.DropDownArrow, "User Settings DropDown");
				
				
		} catch (Exception e) {
			log.log(LogStatus.FAIL, e.getMessage());
			FrameworkFunctions.screenShot("Error"+testCaseName);
		}
		}
	
	//Edit or change the profile data 
	
	public static void Profileupdate (String[] data,String[] assertion) throws HeadlessException, IOException, AWTException {
		
		try {	
			
				testCaseName="Profileupdate";
				log=report.startTest(testCaseName, "This method will use to update the data in user profile page");
				GeneralSettingsNavigation();
				SeleniumFunctions.ClickText(EDControlsRegressionPage.UserSettings, "User Settings");
				SeleniumFunctions.TransferToControlBrowser();
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.FirstName, "First Name");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.FirstName, data[1], "First Name");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.LastName, data[2], "Last Name");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.CompanyName, data[3], "Company Name");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.PhoneNumber, data[4], "Phone Number");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.Address, data[5], "Address ");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.City, data[6], "City ");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.Country, data[7], "Country ");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.PostalCode, data[8], "PostalCode");
				SeleniumFunctions.SelectItemFromListboxByValue(EDControlsRegressionPage.LanguageList, data[0], "DropDown Item for LanguageList is Selected ");
				SeleniumFunctions.SelectItemFromListboxByValue(EDControlsRegressionPage.ProjectUpdates, data[9], "DropDown Item For ProjectUpdates is Selected ");
				SeleniumFunctions.SelectCheckBox(EDControlsRegressionPage.CheckBoxForReceiveEmail, "Check Box For Receive Email");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.SaveButton, "Save Button");
			
			
		} catch (Exception e) {
			log.log(LogStatus.PASS, e.getMessage());
			FrameworkFunctions.screenShot("Error"+testCaseName);
		}
		}
	
	
	// helpDesk page Navigation
	
	public static void HelpDeskFunction () throws HeadlessException, IOException, AWTException {
		
		try {
			
				testCaseName="HelpDeskFunction";
				log=report.startTest(testCaseName, "This method will Click on HelpDesk and navigate to helpDesk Page");
				GeneralSettingsNavigation();
				String oldTab = driver.getWindowHandle();
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.HelpDeskLInk	, "HelpDesk Link");
				SeleniumFunctions.TransferToControlBrowser();
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.HelpDesk, "Help Desk");
				driver.close();
				driver.switchTo().window(oldTab);
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.DropDownArrow, "Drop Down Arrow");
			
			
		} catch (Exception e) 
		{
				log.log(LogStatus.FAIL, e.getMessage());
				FrameworkFunctions.screenShot("Error"+testCaseName);
		}
		}
	
	//Switch User Function 
	
	public static void SwitchToNewUser(String[] data,String[] assertion) throws HeadlessException, IOException, AWTException {
		try {
				
				testCaseName="SwitchToNewUser";
				log=report.startTest(testCaseName, "This method will help user to switch to new user by providing email id");
				GeneralSettingsNavigation();
				SeleniumFunctions.ClickText(EDControlsRegressionPage.SwitchUser, "Switch User");
				SeleniumFunctions.TransferToControlBrowser();
				SeleniumFunctions.EnterText(EDControlsRegressionPage.SwitcherEmailId, data[0], "Switch User Email Id");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.OKButton, "OK button");
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.SwitchedUserID, "Switched User ID");
			
		} catch (Exception e) {
			log.log(LogStatus.FAIL, e.getMessage());
			FrameworkFunctions.screenShot("Error"+testCaseName);
		}
		}
	
	// Ticket module validation 
	
	public static void DrawingSessionSideMenu () throws HeadlessException, IOException, AWTException {
		try {
			
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.Sidemenubutton, "Drawing Side Menu");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.Sidemenubutton, "Drawing Side Menu");
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.Drawing, "Drawing");
			
		} catch (Exception e) {
			log.log(LogStatus.FAIL, e.getMessage());
			FrameworkFunctions.screenShot("Error"+testCaseName);
		}
		}
	
	
	public static void SelectingprojectToCreateTicket (String[] data,String[] assertion) throws HeadlessException, IOException, AWTException {
		
		try {
			
				testCaseName="TicketModuleValidation";
				log=report.startTest(testCaseName, "This method will help user to Check functionality of project selection to create ticket");
				DrawingSessionSideMenu();
				SeleniumFunctions.container(EDControlsRegressionPage.DrawingList, data[0]);	
				Thread.sleep(2000);
				SeleniumFunctions.container(EDControlsRegressionPage.SubDrawingList, data[1]);
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.NewTicketCreation, "New Ticket Creation");
				
		} catch (Exception e) {
			log.log(LogStatus.FAIL, e.getMessage());
			FrameworkFunctions.screenShot("Error"+testCaseName);
		}
		}
	
	
	
		
	// Creating new ticket 
	public static void NewTicketCreation(String[] data,String[] assertion) throws HeadlessException, IOException, AWTException {
		
	try {
		
			testCaseName="NewTicketCreation";
			log=report.startTest(testCaseName, "This method will help user to  Create new Ticket");
			SeleniumFunctions.ClickButton(EDControlsRegressionPage.NewTicketButton, "New Ticket");
			SeleniumFunctions.ClickButton(EDControlsRegressionPage.NewTicketCreation, "New Ticket Creation");
			SeleniumFunctions.TransferToControlBrowser();
			SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.NewTicketPopUp, "New Ticket PopUp");
			SeleniumFunctions.EnterText(EDControlsRegressionPage.ResponsibleFornewTicket, data[0], "Responsible For New Ticket");
			SeleniumFunctions.ClickButton(EDControlsRegressionPage.NewTicketDueDate,"Due Date");
			SeleniumFunctions.CalendarDatePicker(EDControlsRegressionPage.DateSelector, data[1]);
			SeleniumFunctions.EnterText(EDControlsRegressionPage.NewTicketTitle, data[2], "NewTicketTitle");
			SeleniumFunctions.EnterText(EDControlsRegressionPage.NewTicketDescription, data[3], "New Ticket Description");
			SeleniumFunctions.ClickButton(EDControlsRegressionPage.SelectFile, "Select File");
			Thread.sleep(2000);
			UploadFile();
			SeleniumFunctions.ClickButton(EDControlsRegressionPage.NewTicketSaveButton, "New Ticket Save ");
			
			
	} catch (Exception e) {
		log.log(LogStatus.FAIL, e.getMessage());
		FrameworkFunctions.screenShot("Error"+testCaseName);
	}
	
	
	
	}
	
	
	// Created Ticket Functionality 
	
	public static void VerifyCreatedTicketFunction(String[] data,String[] assertion) throws HeadlessException, IOException, AWTException {
		try {
			testCaseName="VerifyCreatedTicketFunction";
			log=report.startTest(testCaseName, "This method will help user to  verify and identify created new Ticket");
			Thread.sleep(2000);
			SeleniumFunctions.ClickButton(EDControlsRegressionPage.NewTicketListNavigator, "New Ticket List Navigator");
			SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.CreatedTicketSelection, "Created Ticket");
			SeleniumFunctions.container(EDControlsRegressionPage.CreatedTicketSelection, data[0]);	
			SeleniumFunctions.ClearText(EDControlsRegressionPage.EnterTitle, "Title");
			SeleniumFunctions.EnterText(EDControlsRegressionPage.EnterTitle, data[1], "Title");
			SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.CreatedTicketSaveButton, "Save Button");
			SeleniumFunctions.ClickButton(EDControlsRegressionPage.CreatedTicketSaveButton, "Save");
			
		} catch (Exception e) {
			log.log(LogStatus.FAIL, e.getMessage());
			FrameworkFunctions.screenShot("Error"+testCaseName);
			
		}
	}
	
	// upload file by using sikuli 
	
	public static void UploadFile() throws HeadlessException, IOException, AWTException {
		try {
			
			
				Pattern Desktop = new Pattern(path +"/Desktop/Screenshots/1.png");
				Pattern Open = new Pattern(path +"/Desktop/Screenshots/2.png");
				Pattern Image = new Pattern(path +"/Desktop/Screenshots/3.png");
				Screen s = new Screen();
				Thread.sleep(2000);
				s.click(s.wait("/home/harsha/Desktop/Screenshots/1.png", 10));
				s.click(Desktop);	
				Thread.sleep(2000);
				s.click(Image);
				Thread.sleep(2000);
				s.click(Open);
				log.log(LogStatus.PASS, "Map Uploaded");
			
			
		}
		
		
		catch (Exception e) {
			log.log(LogStatus.FAIL, e.getMessage());
			System.out.println(e);
			FrameworkFunctions.screenShot("Error"+testCaseName);
		}
	}
	
	// Verify the Ticket Creation
	
	public static void VerifyTicketCreation () {
		try {
				
				String ClickCreatedTicket="catche";
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.NewTicketListNavigator, "New Ticket List Navigator");
				 SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.CreatedTicketSelection, "Created Ticket");
					List<WebElement> element = driver.findElements(EDControlsRegressionPage.CreatedTicketSelection);
				    Iterator<WebElement> i = element.iterator();
				    while(i.hasNext()){
				     WebElement row = i.next();
				   
				      if(row.getText().equals(ClickCreatedTicket))
				      {
					       row.click();
					       System.out.println(row.getText());
					        break; 
					   }
				      System.out.println(row.getText());
			
			
		}
			    log.log(LogStatus.PASS, ClickCreatedTicket + " Is Clicked");
			    SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.CreatedTicketSaveButton, "Save Button");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.CreatedTicketSaveButton, "Save");
				
		}
		catch (Exception e) {
			
		}
	}
	
	// Audits Module automation 
	
	public static void DrawingGroupsSideMenu () throws HeadlessException, IOException, AWTException {
		try {
			
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.DrawingSidemenubutton, "Drawing Side Menu");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.DrawingSidemenubutton, "Drawing Side Menu");
					
				
			
		} catch (Exception e) {
			log.log(LogStatus.FAIL, e.getMessage());
			FrameworkFunctions.screenShot("Error"+testCaseName);
		}
		}																
	
	// Creating New Ticket Group
	
	public static void NewDrawingGroupCreation (String[] data,String[] assertion) throws HeadlessException, IOException, AWTException {
		try {
			
				testCaseName="NewDrawingGroupCreation";
				log=report.startTest(testCaseName, "This method will help user to  Create  New Ticket Group");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.DrawingsMainModule, "Drawings Main Module");
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.AddingNewTicketGroup, "Adding NewTicket Group");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.AddingNewTicketGroup, "Adding NewTicket Group");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.GroupName, data[0], "GroupName");
				driver.findElement(EDControlsRegressionPage.GroupName).sendKeys(Keys.ENTER);
				AddFirstDrawing();
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.DrawingName, data[1]);
				SeleniumFunctions.GetTextFromList(EDControlsRegressionPage.DrawingName, data[1]);
							
		} catch (Exception e) {
			log.log(LogStatus.FAIL, e.getMessage());
			FrameworkFunctions.screenShot("Error"+testCaseName);
		}
	}
	

	
	
	// Drawing module file upload method 
	
		public static void DrawingModuleUploadFile() throws HeadlessException, IOException, AWTException {
			try {
			
			
					Pattern Picture = new Pattern(path +"/Pictures/ScreenShots/1.png");
					Pattern Open = new Pattern(path +"/Pictures/ScreenShots/2.png");
					Pattern Image = new Pattern(path +"/Pictures/ScreenShots/4.png");
					Screen s = new Screen();
					s.click(Picture);
					Thread.sleep(2000);
					s.click(Image);
					Thread.sleep(2000);
					s.click(Open);
					log.log(LogStatus.PASS, "Drawing Map Uploaded");
			}
			catch (Exception e) {
				log.log(LogStatus.FAIL, e.getMessage());
			}
			
		}
	
	// Adding First Drawing 
	
	public static void AddFirstDrawing() {
		try {
			
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.AddFirstDrawing, "Add First Drawing");
				DrawingModuleUploadFile();
				driver.navigate().refresh();
							
			
		} catch (Exception e) {
			
			log.log(LogStatus.FAIL, e.getMessage());
		}
	}
	
	
	// Template functionality 
	public static void NewTemplateCreation(String[] data,String[] assertion) {
		try {
			
				testCaseName="NewTemplateCreation";
				log=report.startTest(testCaseName, "This method will help user to  Create  New Template Group and create new template Q&A");
				String Answer = "Yes/No/NA";// these strings are input in excel 
				String Answer1 ="Single";
				String Answer2 ="Multiple";
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.TemplateMainModule, "Template main module");
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.TemplateNewGroupCreationButton, "Template Creation Button");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.TemplateNewGroupCreationButton, "Template Creation");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.TemplateNewGroupName, data[0], "Template New Group Name");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.NewTemplateButton, "New Template");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.TemplateTitle, data[1], "Template Title");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.TemplateQuestion, data[2], "Template Questions");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.EditCategoryTitle, "Category Title ");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.CategoryTitleTextBox, data[3], "Category Title Text Box");
				if(Answer1.equalsIgnoreCase(data[4])) {
					
					SeleniumFunctions.SelectItemFromListboxByValue(EDControlsRegressionPage.TemplateAnswerDropDown, "3", "Template Answer");
					SeleniumFunctions.EnterText(EDControlsRegressionPage.MultipleAnswerSelection1, "A", "Multiple Answer Selection1");
					SeleniumFunctions.EnterText(EDControlsRegressionPage.MultipleAnswerSelection2, "B", "Multiple Answer Selection2");
				}
				else if (Answer.equalsIgnoreCase(data[4])) {
					
					SeleniumFunctions.SelectItemFromListboxByValue(EDControlsRegressionPage.TemplateAnswerDropDown, "0", "Template Answer");
				}
				else if (Answer2.equalsIgnoreCase(data[4])) {
					
					SeleniumFunctions.SelectItemFromListboxByValue(EDControlsRegressionPage.TemplateAnswerDropDown, "2", "Template Answer");
					SeleniumFunctions.EnterText(EDControlsRegressionPage.MultipleAnswerSelection1, "A", "Multiple Answer Selection1");
					SeleniumFunctions.EnterText(EDControlsRegressionPage.MultipleAnswerSelection2, "B", "Multiple Answer Selection2");
				}
				else {
					
					SeleniumFunctions.SelectItemFromListboxByValue(EDControlsRegressionPage.TemplateAnswerDropDown, "1"	, "Template Answer");
				}
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.Publish, "Publish");
				SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.CreatedTemplateName, "Created New Template Name");
			
			
			
		} catch (Exception e) {
			log.log(LogStatus.FAIL, e.getMessage());
			
		}
		
	}
	public static void AddTemplateForExistingProject(String[] data,String[] assertion)
	{
		try 
		{
			testCaseName="AddTemplateForExistingProject";
			log=report.startTest(testCaseName, "This method will help user to  add new template for existing project");
			String Answer = "Yes/No/NA";// these strings are input in excel 
			String Answer1 ="Single";
			String Answer2 ="Multiple";
			SeleniumFunctions.ClickButton(EDControlsRegressionPage.TemplateMainModule, "Template");
			SeleniumFunctions.container(EDControlsRegressionPage.TemplateContainer,data[0]);
			SeleniumFunctions.ClickButton(EDControlsRegressionPage.ExistingProjectNewTemplateButton, "New Template");
			SeleniumFunctions.EnterText(EDControlsRegressionPage.TemplateTitle, data[1], "Template Title");
			SeleniumFunctions.EnterText(EDControlsRegressionPage.TemplateQuestion, data[2], "Template Questions");
			SeleniumFunctions.ClickButton(EDControlsRegressionPage.EditCategoryTitle, "Category Title ");
			SeleniumFunctions.EnterText(EDControlsRegressionPage.CategoryTitleTextBox, data[3], "Category Title Text Box");
			if(Answer1.equalsIgnoreCase(data[4])) {
				
				SeleniumFunctions.SelectItemFromListboxByValue(EDControlsRegressionPage.TemplateAnswerDropDown, "3", "Template Answer");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.MultipleAnswerSelection1, "A", "Multiple Answer Selection1");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.MultipleAnswerSelection2, "B", "Multiple Answer Selection2");
			}
			else if (Answer.equalsIgnoreCase(data[4])) {
				
				SeleniumFunctions.SelectItemFromListboxByValue(EDControlsRegressionPage.TemplateAnswerDropDown, "0", "Template Answer");
			}
			else if (Answer2.equalsIgnoreCase(data[4])) {
				
				SeleniumFunctions.SelectItemFromListboxByValue(EDControlsRegressionPage.TemplateAnswerDropDown, "2", "Template Answer");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.MultipleAnswerSelection1, "A", "Multiple Answer Selection1");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.MultipleAnswerSelection2, "B", "Multiple Answer Selection2");
			}
			else {
				
				SeleniumFunctions.SelectItemFromListboxByValue(EDControlsRegressionPage.TemplateAnswerDropDown, "1"	, "Template Answer");
			}
			SeleniumFunctions.ClickButton(EDControlsRegressionPage.Publish, "Publish");
			SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.CreatedTemplateName, "Created New Template Name");
					
			
		} 
		catch (Exception e)
		{
					
			log.log(LogStatus.FAIL, e.getMessage());	
					
		}
	}

	
	
	
	
	//Free Text Audits main Module 
	public static void AuditingArea (String[] data,String[] assertion)	{
		
		try {
				testCaseName="AuditingArea";
				log=report.startTest(testCaseName, "This method will help user to  Audit by area and close the audit section of that area");
				String Answer = "Yes/No/NA";// these strings are input in excel 
				String Answer1 ="Multiple";
				String Answer2 ="Single";
			 	SeleniumFunctions.ClickButton(EDControlsRegressionPage.Auditmodule, "Audit");
			 	SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.Templates, "Templates Text");
			 	SeleniumFunctions.container(EDControlsRegressionPage.AuditGroupConatiner, data[2]);
			 	SeleniumFunctions.container(EDControlsRegressionPage.AuditSubGroupConatiner, data[3]);
			 	SeleniumFunctions.ClickImage(EDControlsRegressionPage.SelectionofplaceforAuditing, "Audit place selection");
			 	SeleniumFunctions.ClickText(EDControlsRegressionPage.AreaSelections, "Area");
			 	SeleniumFunctions.ScrollWindow(EDControlsRegressionPage.AreaNextButton, "Area Next Button");			 	
			 	SeleniumFunctions.ClickButton(EDControlsRegressionPage.AreaNextButton, "Area Next ");
			 	if(Answer.equalsIgnoreCase(data[1])) {
			 		
			 		SeleniumFunctions.ClickButton(EDControlsRegressionPage.YesAnswerSelect, "Yes Answer Select");
			 	}
			 	else if (Answer1.equalsIgnoreCase(data[1])) {
			 		
					SeleniumFunctions.SelectCheckBox(EDControlsRegressionPage.AnswerCheckBoxSelected, "Answer CheckBox Selected");
				}
			 	else if (Answer2.equalsIgnoreCase(data[1])) {
			 		
					SeleniumFunctions.ClickRadioButton(EDControlsRegressionPage.AnswerRadioButtonSelected, "Answer Radio Button Selected");
				}
			 	
			 	SeleniumFunctions.EnterText(EDControlsRegressionPage.DescriptionArea, data[0], "Description Of Audit");
			 	SeleniumFunctions.ClickButton(EDControlsRegressionPage.Sign, "Digital Sign ");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.SignArea, "Sign area");
			 	Thread.sleep(2000);
			 	SeleniumFunctions.ClickButton(EDControlsRegressionPage.SignDone, "Sign Done ");
			 	Thread.sleep(2000);
			 	SeleniumFunctions.ScrollWindow(EDControlsRegressionPage.AreaFinishButton, "Area Finish Button");
			 	SeleniumFunctions.ClickButton(EDControlsRegressionPage.AreaFinishButton, "Area Finish ");
			 	SeleniumFunctions.TransferToControlBrowser();
			 	SeleniumFunctions.ClickButton(EDControlsRegressionPage.AuditFinishOkButton, "Audit Finish Ok");
			 	SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.SelectionofplaceforAuditing, "SelectionofplaceforAuditing");
			 	
			 	
			
		} catch (Exception e) {
			log.log(LogStatus.FAIL, e.getMessage());
		}
	}
	
	
	
	// Audit the Object 
	public static void AuditingObject (String[] data,String[] assertion)	{
	
		try {
			
				testCaseName="AuditingObject";
				log=report.startTest(testCaseName, "This method will help user to  Audit by Object and close the audit section of that Object");	
				String Answer = "Yes/No/NA";// these strings are input in excel 
				String Answer1 ="Multiple";
				String Answer2 ="Single";
			 	SeleniumFunctions.ClickButton(EDControlsRegressionPage.Auditmodule, "Audit");
			 	SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.Templates, "Templates Text");
			 	SeleniumFunctions.container(EDControlsRegressionPage.AuditGroupConatiner, data[2]);
			 	SeleniumFunctions.container(EDControlsRegressionPage.AuditSubGroupConatiner, data[3]);
			 	SeleniumFunctions.ClickImage(EDControlsRegressionPage.SelectionofplaceforAuditing, "Audit place selection");
			 	SeleniumFunctions.ClickText(EDControlsRegressionPage.ObjectSelections, "Object Selections");
			 	SeleniumFunctions.container(EDControlsRegressionPage.TickectForObjectAudit, data[4]);
			 	SeleniumFunctions.ScrollWindow(EDControlsRegressionPage.ObjectNextButton, "Object Next  Button");
			 	SeleniumFunctions.ClickButton(EDControlsRegressionPage.ObjectNextButton, "Object Next ");
			 	if(Answer.equalsIgnoreCase(data[1])) {
			 		
			 		SeleniumFunctions.ClickButton(EDControlsRegressionPage.YesAnswerSelect, "Yes Answer Select");
			 	}
			 	else if (Answer1.equalsIgnoreCase(data[1])) {
			 		
					SeleniumFunctions.SelectCheckBox(EDControlsRegressionPage.AnswerCheckBoxSelected, "Answer CheckBox Selected");
				}
			 	else if (Answer2.equalsIgnoreCase(data[1])) {
			 		
					SeleniumFunctions.ClickRadioButton(EDControlsRegressionPage.AnswerRadioButtonSelected, "Answer Radio Button Selected");
				}
			 	
			 	SeleniumFunctions.EnterText(EDControlsRegressionPage.ObjectDescription, data[0], "Description Of Audit");
			 	SeleniumFunctions.ClickButton(EDControlsRegressionPage.Sign, "Digital Sign");
			  	SeleniumFunctions.ClickButton(EDControlsRegressionPage.SignArea, "Sign area");
			 	Thread.sleep(2000);
			 	SeleniumFunctions.ClickButton(EDControlsRegressionPage.SignDone, "Sign Done");
			 	SeleniumFunctions.ScrollWindow(EDControlsRegressionPage.ObjectFinishButton, "Object Finish  Button");
			 	SeleniumFunctions.ClickButton(EDControlsRegressionPage.ObjectFinishButton, "Object Finish");
			 	SeleniumFunctions.TransferToControlBrowser();
			 	SeleniumFunctions.ClickButton(EDControlsRegressionPage.AuditFinishOkButton, "Audit Finish Ok");
			 	SeleniumFunctions.WaitForElementToDisplay(EDControlsRegressionPage.SelectionofplaceforAuditing, "SelectionofplaceforAuditing");
			 	
			
		} catch (Exception e) {
			log.log(LogStatus.FAIL, e.getMessage());
		}
	}
	
	
	
	//Reporter Module 
	public static void CreateReporter(String[] data,String[] assertion) {
		try {
			
				testCaseName="Reporter";
				log=report.startTest(testCaseName, "This method will help user to  Report the things which they think fault");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.Reporter, "Reporter");
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.ReporterCreateButton, "Reporter Create");
				SeleniumFunctions.EnterText(EDControlsRegressionPage.Reportee, data[0], "Reporter");
				SeleniumFunctions.container(EDControlsRegressionPage.SelectDrawingCheckBoxes, data[0]);
				SeleniumFunctions.ClickButton(EDControlsRegressionPage.ReportCreateButton, "Report Create");
				
		} catch (Exception e) {
			log.log(LogStatus.FAIL, e.getMessage());
			
		}
	}


	public static void ReporterCheckBoxSelect()
	{
		try {
				String ClickReportMap="Elephant";
				
					List<WebElement> element = driver.findElements(EDControlsRegressionPage.SelectDrawingCheckBoxes);
				    Iterator<WebElement> i = element.iterator();
				    while(i.hasNext()){
				     WebElement row = i.next();
				      if(row.getText().equals(ClickReportMap))
				      {
					       row.click();
					       
					       System.out.println(row.getText());
					       break;
					       
					   }
				        
				        System.out.println(row.getText());
		    }
				    log.log(LogStatus.PASS, ClickReportMap + " Is Clicked");
		}
		catch (Exception e) 
		{
			log.log(LogStatus.FAIL, e.getMessage());
		}
		}
	
	//Error Validation 
	
	public static void ErrorValidation (String[] data,String[] assertion) {
		try {
			
			testCaseName="ErrorValidation";
			log=report.startTest(testCaseName, " This method will help user to  check all error message  ");
			
		}
		catch (Exception e) {
			log.log(LogStatus.FAIL, e.getMessage());
		}
		
		
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
