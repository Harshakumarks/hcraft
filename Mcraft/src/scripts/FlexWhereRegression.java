package scripts;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.io.IOException;

import org.openqa.selenium.By;

import com.relevantcodes.extentreports.LogStatus;

import commonFunctions.FrameworkFunctions;
import commonFunctions.SeleniumFunctions;
import driver.Driver;
import objectRepository.FlexWhereRegressionPage;

public class FlexWhereRegression extends Driver{
	
	public static void openApplicationURL() throws Exception
	{
		try{
			
				testCaseName="OpenApplicationURL";
				log=report.startTest(testCaseName);
				driver.get(QA_ENVIRONMENT);
				log.log(LogStatus.PASS, "Application url loaded Successfully");
				SeleniumFunctions.WaitForElementToDisplay(FlexWhereRegressionPage.CompanyHeader, "Interactive BluePrints");
				
		}catch(Exception e)
		{
			FrameworkFunctions.screenShot("Error"+testCaseName);
			log.log(LogStatus.FAIL, e.getMessage());
		}
		
		}
	
	
	public static void SelectExistingPremise(String[] data,String[] assertion)  throws Exception
	{
		try {
			
				testCaseName="SelectExistingPremise";
				log=report.startTest(testCaseName);
				SeleniumFunctions.WaitForElementToDisplay(FlexWhereRegressionPage.FirstPremise, "First Premise");
				String FirstTimeBooking = data[0];
				if(FirstTimeBooking.equalsIgnoreCase("Yes")) {
				SeleniumFunctions.WaitForElementToDisplay(FlexWhereRegressionPage.PremisePopUpCloseButton, "Premise PopUp Close");
				SeleniumFunctions.ClickButton(FlexWhereRegressionPage.PremisePopUpCloseButton, "Premise PopUp Close");
				}
				SeleniumFunctions.WaitForElementToDisplay(FlexWhereRegressionPage.FirstPremise, "First Premise");
				SeleniumFunctions.container(FlexWhereRegressionPage.PremiseList, data[0]);
				SeleniumFunctions.WaitForElementToDisplay(FlexWhereRegressionPage.CompanyHeader, "Interactive BluePrints");
				Thread.sleep(4000);       
			        			
		} 
		
		catch (Exception e)
		{
			FrameworkFunctions.screenShot("Error"+testCaseName);
			log.log(LogStatus.FAIL, e.getMessage());
		}
	}
	
	// method is for selecting work place after selecting the required premises.
	public static void FloorSelectionOfSelectedPremise(String[] data,String[] assertion) {
		
		try {
			
				testCaseName="FloorSelectionOfSelectedPremise";
				log=report.startTest(testCaseName);
				SeleniumFunctions.WaitForElementToDisplay(FlexWhereRegressionPage.RequiredFloor, "Floor Occupied Percentage text");
				Thread.sleep(2000);
				SeleniumFunctions.container(FlexWhereRegressionPage.FloorList, data[0]);
				Thread.sleep(2000);
					
			
		}
		
		catch (Exception e) 
		{
			try 
			{
				FrameworkFunctions.screenShot("Error"+testCaseName);
			}
			catch (HeadlessException | IOException | AWTException e1) 
			{
				e1.printStackTrace();
			}
			log.log(LogStatus.FAIL, e.getMessage());
			
		}
	}
	// check for work space booked or not if not book 
		public static void BookWorkSpace(String[] data,String[] assertion) {
			try 
			{
				
				testCaseName="BookWorkSpace";
				log=report.startTest(testCaseName);
				String FirstTimeBooking = data[0];
				if(FirstTimeBooking.equalsIgnoreCase("Yes"))
				{
				
				
					driver.get("https://test-ui.flexwhere.nl/#/premise/Laptop/harsha-PC");
					Thread.sleep(8000);
					SeleniumFunctions.container(FlexWhereRegressionPage.PremiseList, data[1]);
					SeleniumFunctions.WaitForElementToDisplay(FlexWhereRegressionPage.WorkPlaceSelectPopupText, "Work place Select Popup Text");
					Thread.sleep(1000);
					SeleniumFunctions.ClickButton(FlexWhereRegressionPage.WorkPlaceSelectPopupTextCloseButton, "Work Place Select Popup Text Close Button");
					Thread.sleep(1000);
					SeleniumFunctions.WaitForElementToDisplay(FlexWhereRegressionPage.RequiredFloor, "Floor Occupied Percentage text");
					Thread.sleep(2000);
					SeleniumFunctions.container(FlexWhereRegressionPage.FloorList, data[2]);
					Thread.sleep(4000);
					SeleniumFunctions.NewWorkPlaceSelectorInFlexwhere(FlexWhereRegressionPage.WorkSpaceList, data[3]);
					SeleniumFunctions.WaitForElementToDisplay(FlexWhereRegressionPage.BookedPlaceIDInPopUp, "Booked Place ID In PopUp");
		        	SeleniumFunctions.ClickButton(FlexWhereRegressionPage.NewlyBookedPlacePopUpCloseButton, "Booked Place PopUp Close ");
		        	Thread.sleep(3000);
		        	SeleniumFunctions.WaitForElementToDisplay(FlexWhereRegressionPage.CompanyHeader, "Interactive BluePrints");
					SeleniumFunctions.VerifyWorkPlaceSelectInFlexwhere(FlexWhereRegressionPage.WorkSpaceList, data[3]);
				}
				else
				{
					driver.get("https://test-ui.flexwhere.nl/#/premise/Laptop/harsha-PC");
					Thread.sleep(4000);
					SeleniumFunctions.container(FlexWhereRegressionPage.PremiseList, data[1]);
					SeleniumFunctions.WaitForElementToDisplay(FlexWhereRegressionPage.RequiredFloor, "Floor Occupied Percentage text");
					Thread.sleep(2000);
					SeleniumFunctions.container(FlexWhereRegressionPage.FloorList, data[2]);
					Thread.sleep(2000);
					SeleniumFunctions.WorkPlaceSelectorInFlexwhere(FlexWhereRegressionPage.WorkSpaceList, data[3]);
					SeleniumFunctions.WaitForElementToDisplay(FlexWhereRegressionPage.BookedPlaceIDInPopUp, "Booked Place ID In PopUp");
		        	SeleniumFunctions.ClickButton(FlexWhereRegressionPage.BookedPlacePopUpCloseButton, "Booked Place PopUp Close ");
		        	Thread.sleep(2000);
		        	SeleniumFunctions.WaitForElementToDisplay(FlexWhereRegressionPage.CompanyHeader, "Interactive BluePrints");
					SeleniumFunctions.VerifyWorkPlaceSelectInFlexwhere(FlexWhereRegressionPage.WorkSpaceList, data[3]);
					
				}
			}
		
			catch (Exception e)
			{
				e.printStackTrace();
				log.log(LogStatus.FAIL, e.getMessage());
			}
		}	
		
	// if user have default premises then he can select place in bay
		public static void DefaultPremisesfloorSlection(String[] data,String[] assertion) {
			
			try {
			
					testCaseName="DefaultPremisesfloorSlection";
					log=report.startTest(testCaseName);
					SeleniumFunctions.WaitForElementToDisplay(FlexWhereRegressionPage.RequiredFloor, "Floor Occupied Percentage text");
					Thread.sleep(2000);
					SeleniumFunctions.container(FlexWhereRegressionPage.FloorList, data[0]);
					Thread.sleep(2000);
					
				
			}
			
			catch (Exception e) {
				try {
					FrameworkFunctions.screenShot("Error"+testCaseName);
				} catch (HeadlessException | IOException | AWTException e1) {
					e1.printStackTrace();
				}
				log.log(LogStatus.FAIL, e.getMessage());
				
			}
	}
		
	//check the place is reserved for organization use
		public static void CheckWorkPlaceIsInUse(String[] data,String[] assertion) 
		{
			try 
			{
				testCaseName="CheckWorkPlaceIsInUse";
				log=report.startTest(testCaseName);
				SeleniumFunctions.VerifyWorkPlaceSelectInFlexwhere(FlexWhereRegressionPage.WorkSpaceList, data[0]);
				
			} catch (Exception e) 
			
			{
				log.log(LogStatus.FAIL, e.getMessage());
				e.printStackTrace();
			}
		}


	// Booking Meeting Room available in Floor 
		
		public static void MeetingRoomFunctionality(String[] data,String[] assertion)
		{
			try
			{
				testCaseName="MeetingRoomFunctionality";
				log=report.startTest(testCaseName);
				SeleniumFunctions.WorkPlaceSelectorInFlexwhere(FlexWhereRegressionPage.WorkSpaceList, data[0]);
				SeleniumFunctions.WaitForElementToDisplay(FlexWhereRegressionPage.MeetingRoomTimeSelectPopup, "Meeting Room Time Select Popup");
				SeleniumFunctions.TransferToControlBrowser();
				SeleniumFunctions.container(FlexWhereRegressionPage.TimeList, "Time List");
				SeleniumFunctions.VerifyWorkPlaceSelectInFlexwhere(FlexWhereRegressionPage.WorkSpaceList, data[0]);
				SeleniumFunctions.TransferToControlBrowser();
				SeleniumFunctions.WaitForElementToDisplay(FlexWhereRegressionPage.MeetingRoomListLink, "Meeting Room List Link");
				
				
			} 
			
			catch (Exception e)
			{
				e.printStackTrace();
				log.log(LogStatus.FAIL, e.getMessage());
			}
			
			
		}



		// search validation of booked person

		public static void SearchValidation(String[] data,String[] assertion) 
		
		{
			
			
			
			
			
			
			
			
			
		}

























	

}
