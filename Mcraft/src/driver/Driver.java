package driver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.safari.SafariDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.NetworkMode;

import commonFunctions.SeleniumFunctions;
import objectRepository.EmailPage;


public class Driver {
	
	
		private static Logger logger= Logger.getLogger(Driver.class);
		
		public static WebDriver driver;
		public static String browserName ="";
		public static String TestType="";
		public static String QA_ENVIRONMENT="";
		public static String DEV_ENVIRONMENT="";
		public static String PRODUCTION_ENVIRONMENT="";
		public static String testSuiteName="";
		public static String testCaseName="";
		public static String Result="";
		public static String testData="";
		public static String assertions="";
		public static int numberOfParams=0;
		
		public static String TestScenariosExcelPath="";
		public static String GeckoDriverPath="";
		public static String ChromeDriverPath="";
		public static String EdgeDriverPath="";
		public static String InternetExplorerDriverPath="";
		public static String TestReportsPath="";
		public static String AppScreenShotsPath="";
		public static String ExtentReportPath="";
		public static ExtentReports report;
		public static ExtentTest log;
		public static String ExtentConfigPath="";
		public static String MacGeckoDriverPath="";
		public static String MacChromeDriverPath="";
		public static String WindowsChromeDriverPath="";
		public static String WindowsGeckoDriverPath="";
		public static String path =System.getProperty("user.home");
		
		//Execution starts from here 
		public static void main(String args[]) throws Exception
		{
			PropertyConfigurator.configure("log4j.properties");
			logger.info("*******************************************************");
			logger.info("------------WELCOME TO MOBINIUS CRAFT FRAMEWORK -----------");
			logger.info("*******************************************************");
			logger.info("------------------Its a MAIN Class---------------------");
			
			//Read data From TestEnvironments.properties file
			try {
				
			FileReader reader;
			reader = new FileReader("Configuration.properties");
			Properties properties =new Properties();
			properties.load(reader);
			QA_ENVIRONMENT=properties.getProperty("Configuration.QA_ENVIRONMENT");
			DEV_ENVIRONMENT=properties.getProperty("Configuration.DEV_ENVIRONMENT");
			PRODUCTION_ENVIRONMENT=properties.getProperty("Configuration.PRODUCTION_ENVIRONMENT");
			TestScenariosExcelPath=properties.getProperty("Configuration.TestScenariosExcelPath");
			GeckoDriverPath=properties.getProperty("Configuration.GeckoDriverPath");
			EdgeDriverPath=properties.getProperty("Configuration.EdgeDriverPath");
			ChromeDriverPath=properties.getProperty("Configuration.ChromeDriverPath");
			InternetExplorerDriverPath=properties.getProperty("Configuration.InternetExplorerDriverPath");
			TestReportsPath=properties.getProperty("Configuration.TestReportsPath");
			AppScreenShotsPath=properties.getProperty("Configuration.AppScreenShotsPath");
			ExtentReportPath=properties.getProperty("Configuration.ExtentReportPath");
			ExtentConfigPath=properties.getProperty("Configuration.ExtentConfigPath");
			MacGeckoDriverPath=properties.getProperty("Configuration.MacGeckoDriverPath");
			MacChromeDriverPath=properties.getProperty("Configuration.MacChromeDriverPath");
			WindowsChromeDriverPath=properties.getProperty("Configuration.WindowsChromeDriverPath");
			WindowsGeckoDriverPath=properties.getProperty("Configuration.WindowsGeckoDriverPath");
			
			} catch (IOException e) {
				System.out.println(e);
				e.printStackTrace();
			}
			//Initiate Extent Report 
			ExtentReportInit();
			
			//Initialize browser for application under test
			browserInitialization();
			
			//Read execution scenarios from excel file and trigger automation test cases to run
			executeScenarios();
			//SendEmailReports();
			
		}
			
		
		
		/*
		 * method:ExtentreportInitiate
		 * description:Read and write Extent reports
		 * 
		 */
	
		public static Calendar calDate = Calendar.getInstance();
		public static Date currentDate = calDate.getTime();
		public static SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MMM_dd_HH_mm_ss");
		public static String dateNow = formatter.format(currentDate.getTime());
		
		public static void  ExtentReportInit(){
			report=new ExtentReports(path +ExtentReportPath +"TestResult_" + dateNow + "/FlexWhere.html", true, NetworkMode.ONLINE);			
			report 
			.addSystemInfo("Host Name"," Mobinius")
	        .addSystemInfo("Environment", "QA")
	        .addSystemInfo("User Name", "Harsha Kumar K S ");	
			report.loadConfig(new File(path +ExtentConfigPath));
			}
		
		/*
		 * method:Send Email Reports
		 * description:Automate email and send reports.
		 * 
		 */
		
		public static void SendEmailReports () throws Exception
		{
			
			driver.get("https://mobinius.icewarpcloud.in/webmail/");
			driver.findElement(EmailPage.UserName).sendKeys("harsha.ks@mobinius.com");
			driver.findElement(EmailPage.Password).sendKeys("");
			driver.findElement(EmailPage.SignIn).click();
			Thread.sleep(6000);
			driver.findElement(EmailPage.Compose).click();
			SeleniumFunctions.TransferToControlBrowser ();
			Actions actions = new Actions(driver);
	        actions.moveToElement(driver.findElement(By.xpath("//div[@id='gui.frm_compose.to#container']")));
	        actions.click();
	        actions.sendKeys("kiran.gk@mobinius.com");
	        actions.build().perform();
			driver.findElement(EmailPage.Subject).sendKeys("Automation Report");
			actions.moveToElement(driver.findElement(By.xpath("//td[@id='gui.frm_compose.body#msiebox']")));
			actions.click();
	        actions.sendKeys("Hi ,"
	        		+ 
	       " Please Find an attachment for an automation Report.");
	        actions.build().perform();
	        
	        
			driver.findElement(EmailPage.SendButton).click();
			driver.close();
		}
		
		/*
		 * method:browserInitialization
		 * description:Read browser name to launch from excel file and initialize the same.
		 * 
		 */
		
		public static void browserInitialization() {
			
			try{
				
				logger.info("---------Browser Initialization Started----------");
				 String os = System.getProperty("os.name").toLowerCase();
				String excelPath=path +TestScenariosExcelPath;
				FileInputStream input = new FileInputStream(excelPath);
				XSSFWorkbook workbook=new XSSFWorkbook(input);
				XSSFSheet browserSheet = workbook.getSheet("browser");
				int numberOfRows=browserSheet.getLastRowNum();
				
				for(int i=0;i<=numberOfRows;i++)
				{
					XSSFRow row=browserSheet.getRow(i);
					if(row!=null)
					{
						if(row.getCell(2).getStringCellValue().equalsIgnoreCase("yes"))
						{
												
							browserName=row.getCell(1).getStringCellValue().toString();
							logger.info("---------Selected Browser for AUT is : "+browserName +"----------");
							
							
							if(browserName.equalsIgnoreCase("FireFox"))
							{
								if(os.equals("linux")) {
									
								System.setProperty("webdriver.firefox.marionette",path +GeckoDriverPath);
								
								}
								else if (os.equalsIgnoreCase("mac")) {
									
								System.setProperty("webdriver.firefox.marionette",path +MacGeckoDriverPath);
								
								}
								else if (os.equalsIgnoreCase("windows")) {
									
									System.setProperty("webdriver.firefox.marionette",path +WindowsChromeDriverPath);	
								}
								driver=new FirefoxDriver();
								System.out.println("Firefox Selected");								
								driver.manage().window().maximize();
								
								
								}
							
								else if(browserName.equalsIgnoreCase("Chrome")){
									
								if(os.equalsIgnoreCase("linux")) {
									
									System.setProperty("webdriver.chrome.driver",path + ChromeDriverPath);
									
									}
								
								else if (os.equalsIgnoreCase("mac")) {
									
									System.setProperty("webdriver.chrome.driver",path +MacChromeDriverPath);
									
									}
								else if (os.equalsIgnoreCase("windows")) {
									
									//	System.setProperty("webdriver.chrome.driver",WindowsGeckoDriverPath);	
									System.setProperty("webdriver.chrome.driver",path + WindowsGeckoDriverPath);
									}
								
								driver=new ChromeDriver();
								System.out.println("Chrome is Selected");	
								driver.manage().window().maximize();
								
							}else if(browserName.equalsIgnoreCase("InternetExplorer")){
								
								System.setProperty("webdriver.ie.driver",path +InternetExplorerDriverPath);
								
								driver=new InternetExplorerDriver();
								System.out.println("InternetExplorer is  Selected");	
								driver.manage().window().maximize();
								
							}else if(browserName.equalsIgnoreCase("MicrosoftEdge")){
								
								System.setProperty("webdriver.edge.driver",path +EdgeDriverPath);
								driver=new EdgeDriver();
								
							}else if(browserName.equalsIgnoreCase("Safari")){
								
								driver=new SafariDriver();
								
							}
											
						}
						
				// device specification		
						
						
						
						
					}
					else{
						System.out.println("There is no data in excel file");
						
					}
				}
				
				workbook.close();

				} catch (IOException e) {
					     e.printStackTrace();
					}
		}

		
		
		/*
		 * method:executeScenarios
		 * description:It calls the method or test case to run if it found "yes" in excel.
		 */
			
			
		public static void executeScenarios() {
			
			try {
				
				
				logger.info("---------Executing Test Scenarios Started----------");
				
				String excelPath=path +TestScenariosExcelPath;
				FileInputStream input = new FileInputStream(excelPath);
				XSSFWorkbook workbook=new XSSFWorkbook(input);
				
				//GetTestEnvironmentToRun
				XSSFSheet EnvironmentSheet = workbook.getSheet("TestType");
				int numberOfRowsinEnvironmentSheet=EnvironmentSheet.getLastRowNum();
				for(int i=0;i<=numberOfRowsinEnvironmentSheet;i++)
				{
					XSSFRow EnvironmentSheetRow=EnvironmentSheet.getRow(i);
					if(EnvironmentSheetRow!=null)
					{
						if(EnvironmentSheetRow.getCell(2).getStringCellValue().equalsIgnoreCase("yes"))
						{
							TestType=EnvironmentSheetRow.getCell(1).getStringCellValue().toString().trim();
							logger.info("---------Executing Test Type : "+TestType+"----------");
							System.out.println("TestType :"+TestType);
						}
					}
				}
				
				//Go to TestType Sheet
				
				XSSFSheet Sheet = workbook.getSheet(TestType);
				int numberOfRows=Sheet.getLastRowNum();
			
				for(int i=0;i<=numberOfRows;i++)
				{
					XSSFRow row=Sheet.getRow(i);
					if(row!=null)
					{
						if(row.getCell(3).getStringCellValue().equalsIgnoreCase("yes"))
						{
							testSuiteName=row.getCell(1).getStringCellValue().toString().trim();
							testCaseName=row.getCell(2).getStringCellValue().toString().trim();
							
							testData=row.getCell(4).getStringCellValue().toString().trim();
							String DataArray[]=testData.split(",");
							
							assertions=row.getCell(5).getStringCellValue().toString().trim();
							String AssertionsArray[]=assertions.split(",");
							
							//numberOfParams = testData .replaceAll("[^,]","").length();  
							numberOfParams=testData.length();
							System.out.println("testSuiteName: "+testSuiteName);
							System.out.println("testCaseName: "+testCaseName);
							System.out.println("numberOfParams: "+numberOfParams);
							System.out.println("testData: "+testData); 
							System.out.println("assertions: "+assertions); 
							
							logger.info("-----------Running Test Suite :"+testSuiteName+"||"+" Running Test Case :" +testCaseName+ "-------------");
							
							//call to testCase using java reflection
							Class<?> clazz = Class.forName("scripts."+testSuiteName);
							System.out.println("Class Name : "+clazz);
							

							
							if(numberOfParams==0)
							{
								Method m=clazz.getDeclaredMethod(testCaseName);
								System.out.println("Method Name : "+m);
								Object t = clazz.newInstance();
								Object o= m.invoke(t);
								
							} else
							
							{
								//Method m=clazz.getDeclaredMethod(testCaseName, new Class[] {String[].class, String[].class});--> for Key(locators) and value (Data)
								Method m=clazz.getDeclaredMethod(testCaseName, String[].class, String[].class);
								System.out.println("Method Name : "+m);
								Object t = clazz.newInstance();
								Object o= m.invoke(t,new Object[]{DataArray,AssertionsArray});
							}
							
							
						}
						
					}else{
						System.out.println("File is empty");
					}
				}
				
				
				workbook.close();
				report.endTest(log);
				report.flush();
				driver.get("file:///"+path+"/Desktop/interactive_blueprints/Mcraft/McraftRepo/Reports/" +"TestResult_" + dateNow + "/FlexWhere.html");
				Thread.sleep(10000);
				driver.quit();
			}

			catch (Exception e) {
				System.out.println(e);
			}
			
		}
		
}

		