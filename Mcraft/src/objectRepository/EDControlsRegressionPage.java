package objectRepository;

import org.openqa.selenium.By;



public class EDControlsRegressionPage {
	
	//User Login
	public static By UserName =By.xpath("//input[@id='user-name']");
	public static By Password =By.xpath("//input[@id='passwd']");
	public static By LogIn =By.xpath("//div[@id='butn-login']");
	
	//User Registration
	public static By SignUp =By.id("nav-signup");
	public static By SignUpNewEmailID =By.xpath("//*[@id='signup-email']");
	public static By SignUpNewPassWord =By.id("signup-passwd"	);
	public static By SignUpConfirmPassWord =By.id("signup-re-passwd");
	public static By SignUpFirstName =By.id("signup-first-name");
	public static By SignUplastName	=By.id("signup-last-name");
	public static By SignUpCompanyName =By.id("signup-compny");
	public static By SignUpContactNumber =By.id("signup-contact");
	public static By SignUpAddress =By.id("signup-address");
	public static By SignUpCity =By.id("signup-city");
	public static By SignUpPostalCode =By.id("signup-postal-code");
	public static By SignUpCountry=By.id("signup-country");
	public static By NewRegisterSignUp =By.id("butn-signup");
	public static By DoneButton =By.id("butn-signup");
	
	//creating project 
	
	public static By Newproject	=By.xpath("//button[@id='create-new-project']");
	public static By ProjectName =By.xpath("//input[@id='new-project-name']");
	public static By SelectTheContract =By.id("select-Contract");
	public static By Location =By.xpath("//input[@id='new-project-location']");
	public static By Accountable =By.id("new-project-accounatble");
	public static By CreateButton =By.xpath("//*[@id='butn-add-new-project']");
	public static By OKbuttonInPopup =By.xpath("//a[contains(text(),'OK')]");
	
	//Selecting existed project
	
	public static By ExistingProject =By.xpath("//p[contains(text(),'AutomationTestProject')]"); // if u wanna select any other project edit that xpath here
	public static By Drawing = By.xpath("//label[contains(text(),'Drawings')]");
	
	//search for existing project 
	public static By SearchedProject =By.xpath("//p[contains(text(),'AutomationProject')]");// if u wanna Search any other project edit that xpath here
	public static By searchedTextClear =By.cssSelector(".ed-clr-search");
	//User Settings
	public static By DropDownArrow =By.id("mh-current-user");
	public static By UserSettings =By.id("edit-cur-user-info");
	public static By FirstName	=By.id("userFirstName");
	public static By LastName = By.id("userLastName");
	public static By CompanyName = By.id("userCompanyName");
	public static By PhoneNumber = By.id("userPhoneNo");
	public static By Address = By.xpath("//textarea[@type='text']");
	public static By City = By.id("userCity");
	public static By Country = By.id("userCountry");
	public static By PostalCode = By.id("userPostalCode");
	public static By LanguageList = By.cssSelector(".fname-val.language-update>select");
	public static By ProjectUpdates = By.cssSelector(".fname-val.email-update>select");
	public static By CheckBoxForReceiveEmail =By.xpath("//input[@type='checkbox']");
	public static By SaveButton =By.id("save-edit");
	
	//helpDesk
	
	public static By HelpDesk =By.xpath("//h1[contains(text(),'HELPDESK')]");
	public static By HelpDeskLInk =By.xpath("//a[@href='http://www.edcontrols.com/helpdesk-2']");
	
	//Switch user 
	
	public static By SwitchUser = By.id("mh-switch-user-option");
	public static By SwitcherEmailId =By.id("switch-inp-email-id");
	public static By SwitchBack =By.xpath("	//button[contains(text(),'Switch Back')]");
	public static By OKButton =By.xpath("//button[contains(text(),'OK')]");
	public static By SwitchedUserID =By.id("main-header-user-email");
	
	// Drawing side menu function 
	
	public static By Sidemenubutton =By.xpath("//*[@id='butn-ticket-left']");
	
	
	// Ticket Module Validation
	
	
	public static By DrawingList =By.xpath("//*[@id='ticket-left-accordion']/h3/span");
	public static By SubDrawingList =By.xpath("//*[@id='ticket-left-accordion']/div/ul/li/h5");
	public static By IdentifyMap =By.xpath("//*[@id='ticket-map-wrapper']/div/div/div[2]");
	public static By NewTicketButton =By.id("ed-butn-new-ticket");
	public static By ClickingOnNeWTicketImage=By.cssSelector(".leaflet-tile.leaflet-tile-loaded");
	public static By NewTicketPopUp =By.xpath("//h4[contains(text(),'New ticket')]");	
	public static By ResponsibleFornewTicket =By.xpath("//*[@id='tn-responsible']");
	public static By NewTicketDueDate =By.id("tn-due-date");
	public static By DateSelector =By.xpath("//div[@class='datepicker-days']//td[@class='day']");
	public static By NewTicketTitle =By.id("tn-title");
	public static By NewTicketDescription =By.id("tn-description");
	public static By NewTicketSaveButton =By.id("tn-save-ticket");
	public static By NewTicketCreation =By.xpath("//*[@id='map-container']/div[1]/div[1]/div/div[2]/img[1]");
	// please identify where u want click and change the image count like img [1]
	
	//upload file by sikuli 
	 public static By SelectFile =By.xpath("//label[contains(text(),'Select files')]");
	
	 //Ticket Creation Verification
	 
	 public static By NewTicketListNavigator =By.xpath("//*[@class='breadcrum-a-tag'][2]");
	 public static By CreatedTicketSelection =By.cssSelector(".ticket-list-row-content>h4");
	 public static By EnterTitle =By.xpath("//input[@id='td-title']");
	 public static By CreatedTicketSaveButton =By.id("td-save");
	 
	//Drawings Session :: Drawing groups verification
	 public static By DrawingGroups =By.xpath("//label[contains(text(),'Drawing groups')]");
	 public static By AddingNewTicketGroup =By.xpath("//*[@id='create-new-grp-icon']");
	 public static By DrawingsMainModule= By.xpath("//*[@id='ed-maps']");
	 public static By GroupName =By.xpath("//input[@placeholder='Enter group name']");
	 public static By DrawingListContainer =By.xpath("//*[@id='maps-left-grp-list']/h3//div/p");
	 public static By AddFirstDrawing =By.xpath("//label[contains(text(),'Add the first drawing')]");
	 public static By DrawingName =By.xpath("//div[@class='maps-name-main']/div/div[1]/p");
	
	 // side menu button for Drawings Module  
	 public static By DrawingSidemenubutton =By.id("butn-map-left");
	
	// Template module 
	 public static By TemplateMainModule =By.xpath("//*[@id='ed-templates']");
	 public static By TemplateNewGroupCreationButton=By.xpath("//*[@id='template-left-accordian']/h3[1]/div");
	 public static By TemplateNewGroupName =By.xpath("//input[@placeholder='Enter group name']");
	 public static By NewTemplateButton =By.id("new-group-template");
	 public static By ExistingProjectNewTemplateButton =By.xpath("//*[@id='ed-butn-new_template']");
	 public static By TemplateTitle =By.xpath("//input[@placeholder='Enter template name']");
	 public static By TemplateQuestion =By.xpath("//input[@placeholder='Enter question']");
	 public static By TemplateGroupscontainer =By.xpath("//*[@id='template-left-accordian']/h3/span"); ///// Acceptance test
	 public static By TemplateContainer= By.xpath("//*[@id='template-left-accordian']/h3/div/p");
	// Template question and answer Dropdown
	 
	 public static By TemplateAnswerDropDown =By.cssSelector(".selectpicker.option-selected");
	 public static By Publish =By.id("publish");
	 public static By EditCategoryTitle =By.cssSelector(".edit-pen.nt-category");
	 public static By CreatedTemplateName =By.xpath("//div[@class='col-md-12 col-sm-12 col-xs-12 templates-name']/div/p");
	 public static By AlreadyCreatedTemplate =By.xpath("//p[contains(text(),'')]");
	 public static By CategoryTitleTextBox =By.xpath("//input[@placeholder='Category title']");
	 public static By TemplateSideMenuButton =By.xpath("//*[@class='sidemenu-icon']");
	 public static By YesAnswerSelect =By.xpath("//button[contains(text(),'Yes')]")	;
	 public static By AnswerCheckBoxSelected =By.xpath("(//span[@class='ques-checkbox-img'])[1]");
	 public static By AnswerRadioButtonSelected =By.xpath("(//span[@class='ques-radio-img'])[1]");
	 public static By MultipleAnswerSelection1 =By.xpath("(//input[@placeholder='Enter option'])[1]");
	 public static By MultipleAnswerSelection2 =By.xpath("(//input[@placeholder='Enter option'])[2]");
	 
	 
	// Audit Sections
	 
	 public static By Auditmodule =By.xpath("//*[@id='ed-audits']");
	 public static By AuditGroupConatiner =By.xpath("//*[@id='audit-left-accordion']/h3/span[2]");
	 public static By AuditSubGroupConatiner =By.xpath("//*[@id='audit-left-accordion']/div/ul/li/h5");
	 public static By SelectionofplaceforAuditing =By.cssSelector(".float-plus-icon");
	 public static By AreaSelections =By.xpath("//h5[contains(text(),'Area')]");
	 public static By ObjectSelections =By.xpath("//h5[contains(text(),'Object')]");
	 public static By AreaNextButton =By.xpath("//button[contains(text(),'Next')]");
	 public static By DescriptionArea =By.xpath("//textarea[@placeholder='Description']");
	 public static By Sign =By.xpath("//button[contains(text(),'Sign')]");
	 public static By SignArea =By.xpath("//*[@id='sign-canvas']");
	 public static By SignDone =By.xpath("//*[@id='sign-done']");
	 public static By AreaFinishButton =By.id("ad-finish-edit");
	 public static By ObjectNextButton =By.xpath("//button[contains(text(),'Next')]");
	 public static By ObjectDescription =By.xpath("//textarea[@placeholder='Description']");
	 public static By ObjectFinishButton =By.id("ad-finish-edit");
	 public static By AuditFinishOkButton =By.xpath("//a[contains(text(),'OK')]");
	 public static By Templates =By.xpath("//label[contains(text(),'Templates')]");
	 public static By FinishedAreaAuditText =By.xpath("//h4[contains(text(),'MagicBall')]");
	 public static By ObjectAuditText =By.xpath("//h4[contains(text(),'	')]");
	 public static By TickectForObjectAudit =By.xpath("//*[@id='audit-ticket-wrapper']/div/div[2]/div[2]/h4");
	 
	 //Reporter Sections
	 
	 public static By Reporter =By.id("ed-reporter");
	 public static By ReporterCreateButton =By.id("new-reporter");
	 public static By SelectDrawingCheckBoxes =By.xpath("//*[@id='reporter-list-wrapper']/div/div[2]/div[3]/div/p/label");
	 public static By Reportee =By.xpath("(//input[@type='text'])[2]");
	 public static By ReportCreateButton =By.xpath("//button[contains(text(),'Create')]");
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 


}