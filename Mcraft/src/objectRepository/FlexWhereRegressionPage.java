package objectRepository;

import org.openqa.selenium.By;

public class FlexWhereRegressionPage {
	
	
	//Selecting Premise
	public static By PremiseList =By.xpath("//div[@class='premise_container text-center']/div/div/b");
	public static By PremisePopUpCloseButton =By.xpath("(//button[@class='close'])[2]");
	public static By FirstPremise =By.xpath("//*[@id='1']");
	//selecting Room's available place in mobinius.
	
	public static By MultiplepremisesMap =By.xpath("//*[starts-with(@id,'Floor')]//*[@data-container='body']");
	public static By CompanyHeader =By.xpath("//img[@class='fw_Logo img-responsive']");
	public static By FloorList =By.xpath("//*[@style='unsafe']/span/h5[2]");
	public static By WorkSpaceList =By.xpath("//*[@class='fwsvg-floor fwsvg-state-default']//*[@data-container='body']");
	
	// selecting the place which have default premise.
	
	public static By RequiredFloor =By.xpath("//*[contains(text(),'Huidige bezetting')]");
	
	//booking meeting room
	public static By MeetingRoomTimeSelectPopup =By.xpath("//*[@class='popover meetingroom fade top in']");
	public static By TimeList =By.xpath("//*[@class='meetingroomtime pull-right']/button");
	public static By MeetingRoomOccupiedPopup =By.xpath("//*[@class='popover meetingroomoccupied fade top in']");
	public static By MeetingRoomListLink =By.xpath("//*[contains(text(),'Beschikbaarheid overige ruimtes')]");
	// booking work place
	public static By BookedPlaceIDInPopUp =By.xpath("//div[@class='success']/h4[2]/span");
	public static By NewlyBookedPlacePopUpCloseButton =By.xpath("(//button[@class='close'])[2]");
	public static By BookedPlacePopUpCloseButton =By.xpath("(//button[@class='close'])[3]");
	public static By WorkPlaceSelectPopupText =By.xpath("(//div[@class='modal-body clearfix']/h4)[1]");
	public static By WorkPlaceSelectPopupTextCloseButton =By.xpath("(//button[@class='close'])[2]");
	public static By JaButton =By.xpath("//*[contains(text(),'Ja')]");
}

