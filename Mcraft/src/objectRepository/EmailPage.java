package objectRepository;

import org.openqa.selenium.By;

public class EmailPage {
	
	public static By UserName =By.xpath("//input[@name='iw_username']");
	public static By Password =By.xpath("//input[@id='login_pass']");
	public static By SignIn =By.xpath("//input[@name='_a[login]']"); 
	public static By Compose =By.xpath("//span[contains(text(),'Compose')]");
	public static By To =By.xpath("//div[@id='gui.frm_compose.to#container']");
	public static By Subject =By.xpath("//input[@id='gui.frm_compose.subject#main']");
	public static By EmailBody =By.xpath("//td[@id='gui.frm_compose.body#msiebox']");
	public static By SendButton=By.xpath("//input[@name='gui.frm_compose.x_btn_send#main']");
	public static By Attachment=By.xpath("//input[@value='Attach from Local Disk']");

}
