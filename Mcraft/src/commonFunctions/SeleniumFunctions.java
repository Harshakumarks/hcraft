package commonFunctions;
import driver.Driver;
import objectRepository.FlexWhereRegressionPage;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.python.modules.thread.thread;

import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.support.Color;

	public class SeleniumFunctions extends Driver{
	
		
		/*(01)*/
		// ##############################################################################
		// ### Function Name  : WaitForElementToClickable
		// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
		// ### Change History :
		// ###############################################################################
		
		/**
		 *  Description
		 *  This above method is to make script wait for some time till the selected element is available to click . 
		 *  if element is not available we will get TimeoutException .
		 *  if window closed in middle of the execution we will get NoSuchWindowException.
		 **/
		
	
	public static WebElement WaitForElementToClickable (By elementQuery) throws Exception{
		
			int  Time =60;
			boolean Status =false;
			WebElement element =null;
		try{
			
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			FluentWait<WebDriver> pWait = new FluentWait<WebDriver>(driver)
					.withTimeout(Time, TimeUnit.SECONDS)
					.pollingEvery(250, TimeUnit.MILLISECONDS)
					.ignoring(NoSuchElementException.class, StaleElementReferenceException.class)
					.ignoring(WebDriverException.class);
			element = pWait.until(ExpectedConditions.elementToBeClickable(elementQuery));
			Status =true;
			
		}
		catch(TimeoutException e)
		
		{
			log.log(LogStatus.FAIL, "Element not found browser in Sync" , e.getMessage());
		}
		catch(NoSuchWindowException eW){
			log.log(LogStatus.FAIL, "browser closed during Execution", eW.getMessage());
		}
		return element;
		}
	
	
		/*(02)*/
		// ##############################################################################
		// ### Function Name  : WaitForElementToEditable
		// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
		// ### Change History :
		// ##############################################################################
	
		/**
		 *  Description
		 *  This above method is to make script wait for some time till the selected element is available to edit 
		 *  if element is not available we will get TimeoutException .
		 *  if window closed in middle of the execution we will get NoSuchWindowException.
		 **/
	
	public static WebElement WaitForElementToEditable (By elementQuery) throws Exception{
		
			int  Time =60;
			boolean Status =false;
			WebElement element =null;
		try{
			
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			FluentWait<WebDriver> pWait = new FluentWait<WebDriver>(driver)
					.withTimeout(Time, TimeUnit.SECONDS)
					.pollingEvery(250, TimeUnit.MILLISECONDS)
					.ignoring(NoSuchElementException.class, StaleElementReferenceException.class)
					.ignoring(WebDriverException.class);
			pWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(elementQuery));
			element = pWait.until(ExpectedConditions.elementToBeClickable(elementQuery));
			Status =true;
			
		}
		catch(TimeoutException e)
		
		{
			log.log(LogStatus.FAIL, "Element not found browser in Sync" , e.getMessage());
		}
		catch(NoSuchWindowException eW){
			log.log(LogStatus.FAIL, "browser closed during Execution", eW.getMessage());
		}
		return element;
		}
	
		
		/*(03)*/
		//##############################################################################
		// ### Function Name  : WaitForElementToDisplay
		// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
		// ### Change History :
		// #############################################################################
		
		/**
		 *  Description
		 *  This above method is to check if the given element is located in DOM and Check it is visible or not in webpage
		 *  if its not visible  time out exception will be thrown and same will be reported in execution report 
		 **/
	
	
	public static boolean WaitForElementToDisplay (By elementQuery , String strElementName ) throws Exception
	{
	
		int  Time =60;
		boolean  Status =false;
	try
	{
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		FluentWait<WebDriver> pWait = new FluentWait<WebDriver>(driver)
				.withTimeout(Time, TimeUnit.SECONDS)
				.pollingEvery(250, TimeUnit.MILLISECONDS)
				.ignoring(NoSuchElementException.class, StaleElementReferenceException.class)
				.ignoring(WebDriverException.class);
		pWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(elementQuery));
		Status =true;
		FrameworkFunctions.screenShot(strElementName);
		
	}
	catch (TimeoutException e) {
		log.log(LogStatus.FAIL, "Element not Found"	, e.getMessage());
		
				
	}
	catch (NoSuchWindowException eW) {
		log.log(LogStatus.FAIL, "Browser closed during Execution ",	 eW.getMessage());
	}
	if(Status){
		log.log(LogStatus.PASS, strElementName +"</b> validation and </b>" +   strElementName + " is present ");
	}
	else{
		log.log(LogStatus.FAIL, strElementName +" </b> validation and </b>" + strElementName+ "<b> is not present </b>");
	}
	return Status;
	}

			/*(04)*/
			//##############################################################################
			// ### Function Name  : SelectItemFromListboxByIndex
			// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
			// ### Change History :
			// #############################################################################
			
			/**
			 *  Description
			 *  This above method is to select an element by reading index of an listbox items(values)
			 * 	 *  if window closed in middle of the execution we will get NoSuchWindowException.
			 **/
	
	public  static void SelectItemFromListboxByIndex(By elementQuery , int intIndex, String StrName) throws Exception
		{
		
		try{
			Thread.sleep(2000);
			WebElement element =driver.findElement(elementQuery);
		if(!(element == null)){
			Select select = new Select (element);
			select.selectByIndex(intIndex);
			log.log(LogStatus.PASS, "<b> " + StrName + "<b/> Listbox . " , "<b>" + intIndex  + "</b> Item Selected ."  );
		}
		else {
			log.log(LogStatus.FAIL, "<b> " + StrName + "<b/> Listbox . " , "<b>" + intIndex  + "</b> Item Selected ."  );
		}
		
		}
		catch (Exception e) {
			log.log(LogStatus.FAIL,"Error in selecting List Box " , e.getMessage() );
		}
	
			}
	
			/*(05)*/
			//##############################################################################
			// ### Function Name  : GetText
			// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
			// ### Change History :
			// #############################################################################
		
			/**
			 *  DescriptionWebElement element = WaitForElementToEditable(elementQuery);
			 *  This above method is to get text from the web page or in DOM
			 *  if window closed in middle of the execution we will get NoSuchWindowException.
			 **/
	
		public static String  GetText(By elementQuery , String  StrElementName ) throws  Exception
		{
			
			String StrActualText = " ";
			Thread.sleep(2000);
			WebElement element =driver.findElement(elementQuery);
			if (!(element==null))
			{
				StrActualText =element.getText(); 
				log.log(LogStatus.PASS, "<b> The </b>" + StrActualText +"<b> is Present </b>");
				
			}
			else
			{
				log.log(LogStatus.FAIL, "<b> The </b>" + StrElementName + "<b> Object is not displayed </b>");
			}
			return StrActualText;
			
		}
		
		/*(06)*/
		//##############################################################################
		// ### Function Name  : WaitForElementToDisplayed
		// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
		// ### Change History :
		// #############################################################################
		/**
		 *  Description
		 *  This above method is to wait for particular element for standard time . 
		 *  please use this method inside the methods which are going to create in selenium functions class(use if needed). 
		 *  please check an example method  "ClickImage" 
		 *  if window closed in middle of the execution we will get NoSuchWindowException.
		 **/

		/*public  static WebElement WaitForElementToDisplayed(By elementQuery) {
			
				int  Time =60;
				WebElement  element=driver.findElement(elementQuery);
				try
				{
					driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
							FluentWait<WebDriver> pWait = new FluentWait<WebDriver>(driver)
							.withTimeout(Time, TimeUnit.SECONDS)
							.pollingEvery(250, TimeUnit.MILLISECONDS)
							.ignoring(NoSuchElementException.class, StaleElementReferenceException.class)
							.ignoring(WebDriverException.class);
					pWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(elementQuery));
					
					
				}
				catch (TimeoutException e) {
					log.log(LogStatus.FAIL, "Element not Found"	, e.getMessage());
							
				}
				catch (NoSuchWindowException eW) {
					log.log(LogStatus.FAIL, "Browser closed during Execution ",	 eW.getMessage());
				}
				return element;
				
				}*/
		
		
		
			/*(07)*/
			//##############################################################################
			// ### Function Name  : EnterText
			// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
			// ### Change History :
			// #############################################################################
			/**
			 *  Description
			 *  This above method is used to enter text in text box, search box and password Field etc 
			 *  if element is not available we will get TimeoutException .
			 *  if window closed in middle of the execution we will get NoSuchWindowException.
			 **/
	  	
	public static void EnterText(By elementQuery , String StrValue , String StrName	) throws Exception
	{
		try{
				WebElement element = driver.findElement(elementQuery);
				Thread.sleep(2000);
		if(!(element==null)){
			StrValue= StrValue.trim();
			if(!StrValue.equalsIgnoreCase("~Blank"))
			{
				element.clear();
				element.sendKeys(StrValue);
				
				
			}
			log.log(LogStatus.INFO, " The data value  <b>"+ StrValue + " </b> Entered ");
			FrameworkFunctions.screenShot(StrName);
			
			
			 
		}else {
			log.log(LogStatus.FAIL, " Object Not Found <b>" + elementQuery + "</b> Not FOund" );
				
				
		}
		}
		catch (Exception e) {
			log.log(LogStatus.FAIL, "Error in Entering Text Field ", e.getMessage());
		}
		}
	 	
		/*(08)*/
		//##############################################################################
		// ### Function Name  : TransferToControlBrowser
		// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
		// ### Change History :
		// #############################################################################
		/**
		 *  Description
		 *  This above method is used to Transfer the control from current window to next window  
		 *  if element is not available we will get NoSuchWindowException .
		 *  
		 **/
	
	public static void TransferToControlBrowser ()	throws Exception{
		
		try {
			
			for(String WinHandle :driver.getWindowHandles())
			{
				driver.switchTo().window(WinHandle);
				
			}
			log.log(LogStatus.PASS, "Browser Control Switch ", "Browser Control Switched as Expected "); 
			}
			catch (Exception e) {
				log.log(LogStatus.FAIL, "Browser Control Switch ", e.getMessage());
			}
			}
	
		/*(09)*/
		//##############################################################################
		// ### Function Name  : ClickImage
		// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
		// ### Change History :
		// #############################################################################
		/**
		 *  Description
		 *  This above method is used to Clicking on Image present in Front End web page
		 *  if element is not available we will get ImageNotFoundException  .
		 *  
		 **/
		
	
	public static void ClickImage (By elementQuery , String StrImageName )
	{
		try {
			
			Thread.sleep(2000);
			WebElement element =driver.findElement(elementQuery);
			if(!(element==null))
			{
				FrameworkFunctions.screenShot(StrImageName);
				Thread.sleep(2000);
				element.click();
				log.log(LogStatus.PASS, " <b>" + StrImageName + "</b> Link", "Image Clicked");
				
			}
			
		
		else {
			log.log(LogStatus.FAIL,  " <b>" + StrImageName + "</b> Link", "Image not Exist");
			
		}
		}
		catch (Exception e) {
			
			log.log(LogStatus.FAIL, "Error in Clicking Image", e.getMessage());
			
		}
	}
	
		/*(10)*/
		//##############################################################################
		// ### Function Name  : ClickButton
		// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
		// ### Change History :
		// #############################################################################
		
		/**
		 *  Description
		 *  This above method is used to Clicking on Button present in Front End web page
		 *  if element is not available we will get ButtonNotFoundException  .
		 * @throws AWTException 
		 * @throws IOException 
		 * @throws HeadlessException 
		 *  
		 **/
	public static void ClickButton (By elementQuery , String StrButtonName ) throws HeadlessException, IOException, AWTException
	{
		try {
			
			Thread.sleep(2000);
			WebElement  element = driver.findElement(elementQuery);
			if(!(element==null))
			{
				FrameworkFunctions.screenShot(testCaseName);
				element.click();
				log.log(LogStatus.PASS, " <b>" + StrButtonName + "</b> Button " + " Clicked");
				
				
				
			}
			
		
		else {
			log.log(LogStatus.FAIL,  " <b>" + StrButtonName + "</b> Button " + " not Exist");
			FrameworkFunctions.screenShot("ERROR"+ StrButtonName);
			
		}
		}
		catch (Exception e) {
			
			log.log(LogStatus.FAIL, "Error in Clicking on Button", e.getMessage());
			FrameworkFunctions.screenShot("ERROR"+ StrButtonName);
		}
	}
	
		/*(11)*/
		//##############################################################################
		// ### Function Name  : ClickRadioButton
		// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
		// ### Change History :
		// #############################################################################
		/**
		 *  Description
		 *  This above method is used to Clicking on Radio Button present in Front End web page
		 *  if element is not available we will get ButtonNotFoundException  .
		 * @throws Exception 
		 * @throws IOException 
		 * @throws HeadlessException 
		 *  
		 **/
		
		public static void ClickRadioButton (By elementQuery , String StrRadioButtonName ) throws HeadlessException, IOException, Exception
		{
			try 
			{
				Thread.sleep(2000);
				WebElement element =driver.findElement(elementQuery);
				if(!(element==null))
				{
					Thread.sleep(2000);
					FrameworkFunctions.screenShot(StrRadioButtonName);
					element.click();
					log.log(LogStatus.PASS, " <b>" + StrRadioButtonName + "</b> Radio Button", "Radio button Clicked");
					
				}
				
			
			else 
			{
				log.log(LogStatus.FAIL,  " <b>" + StrRadioButtonName + "</b> Radio Button", "Radio Button not Exist");
				FrameworkFunctions.screenShot("ERROR"+ StrRadioButtonName);
			}
			}
			catch (Exception e) {
				
				log.log(LogStatus.FAIL, "Error in Clicking on Radio Button", e.getMessage());
				FrameworkFunctions.screenShot("ERROR"+ StrRadioButtonName);
			}
		}
		
		/*(12)*/
		//##############################################################################
		// ### Function Name  : MouseOverOnSubMenu
		// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
		// ### Change History :
		// #############################################################################
		/**
		 *  Description
		 *  This above method is used to  Mouse Over On SubMenu present in Front End web page
		 *  if element is not available we will get Normal Exception .
		 * @throws Throwable 
		 * @throws IOException 
		 * @throws HeadlessException 
		 *  
		 **/
	
		public static void MouseOverOnSubMenu(By elementQuery ,String strSubMenu) throws HeadlessException, IOException, Throwable
		{
			try {
				
				Thread.sleep(2000);
				WebElement element =driver.findElement(elementQuery);
				Actions actObj=new Actions(driver);
				actObj.moveToElement(element).build().perform();
				
				log.log(LogStatus.PASS, "<b> MouseOver on " + strSubMenu +"</b>  " , "MouseOver on Submenu is done ");
				
				} 
			catch (Exception e) 
				{
				log.log(LogStatus.FAIL, "<b> Error in MouseOver on " + strSubMenu +"</b>  " , "Error Occured On MouseOver on SubMenu ");
				FrameworkFunctions.screenShot("ERROR"+ strSubMenu);
				}
				}		
			
		/*(13)*/
		//##############################################################################
		// ### Function Name  : Verify Text Value 
		// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
		// ### Change History :
		// #############################################################################
		/**
		 *  Description
		 *  This above method is used to VerifyTextValue present in Front End web page
		 *  if element is not available we will get NoSuchElementException.
		 *  
		 **/
	public static void VerifyTextValue(By elementQuery , String StrTextToVerify)throws Exception
	{
		try {
			
			Thread.sleep(2000);
			WebElement element =driver.findElement(elementQuery);
			if (!(element== null))
			{
				if(element.getText().trim().toUpperCase().equals(StrTextToVerify.trim().toUpperCase()))
					
					log.log(LogStatus.PASS, "Content Displayed",  StrTextToVerify + "text is Displayed");
				else 
					log.log(LogStatus.FAIL, "Content Displayed",  StrTextToVerify + "text is not Displayed");
				FrameworkFunctions.screenShot("ERROR"+ StrTextToVerify);
			}
			else 
			{
				
				log.log(LogStatus.FAIL, "Content not Displayed",  StrTextToVerify + "text is not Displayed");
				FrameworkFunctions.screenShot("ERROR"+ StrTextToVerify);
			}
			
			}
		catch (org.openqa.selenium.NoSuchElementException e) {
			log.log(LogStatus.FAIL, "Content not Displayed",  StrTextToVerify + "text is not Displayed");
			FrameworkFunctions.screenShot("ERROR"+ StrTextToVerify);
			}
	
			}
		
		/*(14)*/
		//##############################################################################
		// ### Function Name  : SelectItemFromListboxByValue
		// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
		// ### Change History :
		// #############################################################################
		/**
		 *  Description
		 *  This above method is used to SelectItemFromListboxByValue present in Front End web page
		 *  if element is not available we will get NoSuchElementException.
		 *  
		 **/
		
	
	public  static void SelectItemFromListboxByValue(By elementQuery , String Value, String StrName) throws Exception
	{
	try{
		
		Thread.sleep(2000);
		WebElement element =driver.findElement(elementQuery);
	if(!(element == null)){
		Select select = new Select (driver.findElement(elementQuery));
		select.selectByValue(Value);
		log.log(LogStatus.PASS, "<b> " + StrName + "<b/> From Listbox .And " , "<b>" + Value  + "</b> Item is Selected ."  );
	}
	else {
		log.log(LogStatus.FAIL, "<b> " + StrName + "<b/> From Listbox . " , "<b>" + Value  + "</b> Item is not Selected ."  );
	}
	
	}
	catch (Exception e) {
		log.log(LogStatus.FAIL,"Error in selecting List Box " , e.getMessage() );
	}

		}
	
	
		/*(15)*/
		//##############################################################################
		// ### Function Name  : SelectItemFromListboxByVisibleText
		// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
		// ### Change History :
		// #############################################################################
		/**
		 *  Description
		 *  This above method is used to SelectItemFromListboxByVisibleText present in Front End web page
		 *  if element is not available we will get NoSuchElementException.
		 *  
		 **/
		
	
	public  static void SelectItemFromListboxByVisibleText(By elementQuery , String VisibleName, String StrName) throws Exception
	{
	try{
		
		Thread.sleep(2000);
		WebElement element =driver.findElement(elementQuery);
	if(!(element == null)){
		Select select = new Select (element);
		select.selectByValue(VisibleName);
		log.log(LogStatus.PASS, "<b> " + StrName + "<b/> From Listbox . And" , "<b>" + VisibleName  + "</b> Item is Selected ."  );
	}
	else {
		log.log(LogStatus.FAIL, "<b> " + StrName + "<b/> From Listbox . " , "<b>" + VisibleName  + "</b> Item is not Selected ."  );
	}
	
	}
	catch (Exception e) {
		log.log(LogStatus.FAIL,"Error in selecting List Box " , e.getMessage() );
	}

		}
	
	
	
	
	
		/*(16)*/
		//##############################################################################
		// ### Function Name  : AcceptAlertpopup
		// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
		// ### Change History :
		// #############################################################################
		/**
		 *  Description
		 *  This above method is used to SelectItemFromListboxByVisibleText present in Front End web page
		 *  if element is not available we will get NoSuchElementException.
		 *  
		 **/
			
	public static void AcceptAlertpopup (By elementQuery , String popupName	) {
		try {
			
				WebElement element =driver.findElement(elementQuery);
				element.click();
				Alert alert = driver.switchTo().alert();
				FrameworkFunctions.screenShot(popupName);
				alert.accept();
				log.log(LogStatus.PASS, "Clicked on ok button in Popup");
				
		}
		catch (Exception e) {
			log.log(LogStatus.FAIL, e.getMessage());
			
		}
		
	}
	
		/*(17)*/
		//##############################################################################
		// ### Function Name  : SelectCheckBox
		// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
		// ### Change History :
		// #############################################################################
		/**
		 *  Description
		 *  This above method is used to Clicking on CheckBox present in Front End web page
		 *  if element is not available we will get ButtonNotFoundException  .
		 * @throws Exception 
		 * @throws IOException 
		 * @throws HeadlessException 
		 *  
		 **/
		
		public static void SelectCheckBox (By elementQuery , String StrCheckboxName ) throws HeadlessException, IOException, Exception
		{
			try 
			{
				Thread.sleep(2000);
				WebElement element =driver.findElement(elementQuery);
				if(!(element==null))
				{
					Thread.sleep(2000);
					FrameworkFunctions.screenShot(StrCheckboxName);
					element.click();
					log.log(LogStatus.PASS, " <b>" + StrCheckboxName + "</b> check Box", "Check Box Selected");
					
				}
				
			
			else 
			{
				log.log(LogStatus.FAIL,  " <b>" + StrCheckboxName + "</b> Check Box", "Check Box is not Exist");
				FrameworkFunctions.screenShot("ERROR"+ StrCheckboxName);
			}
			}
			catch (Exception e) {
				
				log.log(LogStatus.FAIL, "Error in Selecting on Check Box", e.getMessage());
				FrameworkFunctions.screenShot("ERROR"+ StrCheckboxName);
			}
		}
	
			/*(18)*/
			//##############################################################################
			// ### Function Name  : ClickText
			// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
			// ### Change History :
			// #############################################################################
			/**
			 *  Description
			 *  This above method is used to Clicking on text present in Front End web page
			 *  if element is not available we will get ButtonNotFoundException  .
			 * @throws Exception 
			 * @throws IOException 
			 * @throws HeadlessException 
			 *  
			 **/
			
			public static void ClickText (By elementQuery , String StrTextName ) throws HeadlessException, IOException, Exception
			{
				try 
				{
					Thread.sleep(2000);
					WebElement element =driver.findElement(elementQuery);
					if(!(element==null))
					{
						Thread.sleep(2000);
						FrameworkFunctions.screenShot(StrTextName);
						element.click();
						log.log(LogStatus.PASS, " <b> " + StrTextName + "</b> Text Clicked");
						
					}
					
				
				else 
				{
					log.log(LogStatus.FAIL,  " <b>" + StrTextName + "</b> Text", "Text  not Exist");
					FrameworkFunctions.screenShot("ERROR"+ StrTextName);
				}
				}
				catch (Exception e) {
					
					log.log(LogStatus.FAIL, "Error in Clicking on Text", e.getMessage());
					FrameworkFunctions.screenShot("ERROR"+ StrTextName);
				}
			}
		
			/*(19)*/
			//##############################################################################
			// ### Function Name  : ClearText
			// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
			// ### Change History :
			// #############################################################################
			/**
			 *  Description
			 *  This above method is used to Clear text present in Front End web page
			 *  if element is not available we will get ButtonNotFoundException  .
			 * @throws AWTException 
			 * @throws Exception 
			 * @throws IOException 
			 * @throws HeadlessException 
			 *  
			 **/
			public static void ClearText(By elementQuery , String StrFieldName) throws HeadlessException, IOException, AWTException {
				try {
					WebElement element =driver.findElement(elementQuery);
					if(!(element==null))
					{
						Thread.sleep(2000);
						FrameworkFunctions.screenShot(StrFieldName);
						element.clear();
						log.log(LogStatus.PASS, " <b> " + StrFieldName + "</b> Text Cleared");
						
					}
					else 
					{
						log.log(LogStatus.FAIL,  " <b>" + StrFieldName + "</b> Text", "Text  not Exist");
						FrameworkFunctions.screenShot("ERROR"+ StrFieldName);
					}
					
					
				} catch (Exception e) {
					
					log.log(LogStatus.FAIL, "Error in Clearing Text", e.getMessage());
					FrameworkFunctions.screenShot("ERROR"+ StrFieldName);
				}
		
	}
			/*(20)*/
			//##############################################################################
			// ### Function Name  : ScrollWindow
			// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
			// ### Change History :
			// #############################################################################
			/**
			 *  Description
			 *  This above method is used to Scroll Window by identifying element present in Front End web page
			 *  if element is not available we will get Element Not Found Exception  .
			 * @throws AWTException 
			 * @throws Exception 
			 * @throws IOException 
			 * @throws HeadlessException 
			 *  
			 **/
	
			public static void ScrollWindow (By elementQuery, String StrFieldName) throws Exception {
		
				try {
						WebElement element = driver.findElement(elementQuery);
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
						Thread.sleep(2000); 
					
					
				} catch (Exception e) {
					log.log(LogStatus.FAIL, "Error in Scroll in Window", e.getMessage());
					FrameworkFunctions.screenShot("ERROR"+ StrFieldName);
					
				}
	}
	
			/*(21)*/
			//##############################################################################
			// ### Function Name  : CalendarDatePicker
			// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
			// ### Change History :
			// #############################################################################
			/**
			 *  Description
			 *  This above method is used to pick the date in calendar in front-end webpage 
			 *  if element is not available we will get Element Not Found Exception  .
			 * @throws AWTException 
			 * @throws Exception 
			 * @throws IOException 
			 * @throws HeadlessException 
			 *  
			 **/
		
	public static void CalendarDatePicker (By elementQuery, String DateValue ) throws HeadlessException, IOException, AWTException {
		try {
			
			List<WebElement> allDates=driver.findElements(elementQuery);
			
			for(WebElement ele:allDates)
			{
				
				String date=ele.getText();
				
				if(date.equalsIgnoreCase(DateValue))
				{
					ele.click();
					System.out.println(ele);
					break;
				}
				
			}
			log.log(LogStatus.PASS, "Date Selected");
		} catch (Exception e) {
			log.log(LogStatus.FAIL, "Error in Clicking on Date", e.getMessage());
			FrameworkFunctions.screenShot("ERROR"+ DateValue);
			
		}
	}
	
	
		/*(22)*/
		//##############################################################################
		// ### Function Name  : container
		// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
		// ### Change History :
		// #############################################################################
		/**
		 *  Description
		 *  This above method is used to select the container from container list  
		 *  if element is not available we will get Element Not Found Exception  .
		 * @throws AWTException 
		 * @throws Exception 
		 * @throws IOException 
		 * @throws HeadlessException 
		 *  
		 **/
	
	
	public static void container( By elementQuery ,String ContainerName)
	{
		try {
				
					List<WebElement> element = driver.findElements(elementQuery);
				    Iterator<WebElement> i = element.iterator();
				    if(element.size()== 0) {
				    	
				    	System.out.println("Container doesn't have any list");
				    }
				    else
				    {
				    while(i.hasNext()){
				     WebElement row = i.next();
				     //System.out.println(row.getText());
				      if(row.getText().equals(ContainerName))
				      {
					       row.click();
					       log.log(LogStatus.PASS, ContainerName + " Is Clicked");
					       //System.out.println(row.getText());
					       break;
					       
				        }
				      
				     
			    }
				    }    
		}
		catch (Exception e) 
		{
			log.log(LogStatus.FAIL, e.getMessage());
		}
		}
	
	
		/*(23)*/
		//##############################################################################
		// ### Function Name  : GetTextFromList
		// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
		// ### Change History :
		// #############################################################################
		/**
		 *  Description
		 *  This above method is used to select the container from container list  
		 *  if element is not available we will get Element Not Found Exception  .
		 * @throws AWTException 
		 * @throws Exception 
		 * @throws IOException 
		 * @throws HeadlessException 
		 *  
		 **/
	
	public static void GetTextFromList( By elementQuery,String Expected)
	{
		try {
				
					List<WebElement> element = driver.findElements(elementQuery);
				    Iterator<WebElement> i = element.iterator();
				    while(i.hasNext()){
				     WebElement row = i.next();
				    String  Actual=row.getText();
				      if(Actual.equals(Expected))
				      {
					       log.log(LogStatus.PASS, "Actual text is same as expected text ");
					        
					        break;
				        }
				      
				        
				        
			    }
				    
		}
		catch (Exception e) 
		{
			log.log(LogStatus.FAIL, e.getMessage());
		}
		}
	
	
	/*(24)*/
	//##############################################################################
	// ### Function Name  : NewWorkPlaceSelectorInFlexwhere
	// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
	// ### Change History :
	// #############################################################################
	/**
	 *  Description
	 *  This above method is used to select the Place for Booking in FlexWhere In WEB
	 *  if element is not available we will get Element Not Found Exception or element not able to click  .
	 * @throws AWTException 
	 * @throws Exception 
	 * @throws IOException 
	 * @throws HeadlessException 
	 *  
	 **/
	
	
	
	public static void NewWorkPlaceSelectorInFlexwhere(By elementQuery,String ClickingObject) {
		try {
			
				List<WebElement> elements = driver.findElements(elementQuery);
				Iterator<WebElement> i = elements.iterator();
			    while(i.hasNext()) {
			     WebElement element = i.next();
			     System.out.println(element.getAttribute("id") );
				 if(element.getAttribute("id").equalsIgnoreCase(ClickingObject))
		        {
		        	
		        	System.out.println(element.getAttribute("id"));
			        String colourType =	element.getCssValue("fill");
			        System.out.println(element.getCssValue("fill"));
			        System.out.println("The Colour is "  + colourType);
			        String hex = Color.fromString(colourType).asHex();
			        System.out.println(hex);
			        
		        if(hex.equals("#66cc66")) 
		        {
		        	
		        	log.log(LogStatus.INFO, "Colour is green hence place is available for booking.");
		        	Thread.sleep(1000);
		        	element.click();
		        	Thread.sleep(1000);
		        			        	
		        }
		        else if (hex.equals("#cc6666")) 
		        {

		        	log.log(LogStatus.INFO, "Colour is Red which means your colleague is booked this place.Hence place is not available for booking at this time.");
		        }
		        else  if(hex.equals("#cfd8dc")) 

		        {

		        	log.log(LogStatus.INFO, "Colour is grey which means the place is disabled for booking . So please choose other place for book.");

		        }
		        else  if(hex.equals("#004f89"))

		        {

		        	log.log(LogStatus.INFO, "Colour is blue which means the place is fixed for the organization use. So please book the other place.");

		        }
		        else if(hex.equals("#ffff00"))
		        {
		        	log.log(LogStatus.INFO, "Clour is Yellow which means it is available for booking. please start your booking.");
		        	Thread.sleep(1000);
		        	element.click();
		        	Thread.sleep(1000);
		        }
		        else
		        {
		        
		        log.log(LogStatus.FAIL, "Some Error Occured please Check your selected workplace colour ");
		        
		         
		        }
		        break;
		        }
	     
		}
	    
		}
		
		catch (Exception e) 
		{
			
			
			log.log(LogStatus.FAIL, e.getMessage());
			System.out.println(e);
		}
		
	}
	
	
	/*(25)*/
	//##############################################################################
	// ### Function Name  : VerifyWorkPlaceSelectInFlexwhere
	// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
	// ### Change History :
	// #############################################################################
	/**
	 *  Description
	 *  This above method is used to verify the selected  Place after Booking in FlexWhere In WEB
	 *  if element is not available we will get Element Not Found Exception or element not able to click  .
	 * @throws AWTException 
	 * @throws Exception 
	 * @throws IOException 
	 * @throws HeadlessException 
	 *  
	 **/

	
	public static void	VerifyWorkPlaceSelectInFlexwhere(By elementQuery,String ClickingObject) {
		try {
		List<WebElement> elements = driver.findElements(elementQuery);
		Iterator<WebElement> i = elements.iterator();
	    while(i.hasNext()) {
	     WebElement element = i.next();
	    System.out.println(element.getAttribute("id") );
		        if(element.getAttribute("id").equalsIgnoreCase(ClickingObject))
		        {
		        	//WaitForElementToDisplay(elementQuery, "Selected WorkPlace");
		        	System.out.println(element.getAttribute("id"));
			        String colourType =	element.getCssValue("fill");
			        System.out.println("The Colour is " + colourType);
			        String hex = Color.fromString(colourType).asHex();
			        System.out.println(hex);
		        if(hex.equals("#66cc66")) {
		        	
		        	log.log(LogStatus.FAIL, "Colour is green hence place is available for booking which means the booking functionality is not working.");
		                	
		        		        	
		        }
		        else if (hex.equals("#cc6666")) 
		        {
		        	Thread.sleep(2000);
		        	log.log(LogStatus.INFO, "Colour is Red which means The meeting room/workplace Booked");
		        	//element.click();
		        }
		        else  if(hex.equals("#cfd8dc")) 

		        {

		        	log.log(LogStatus.INFO, "Colour is grey which means the place is disabled for booking . So please choose other place for book.");

		        }
		        else  if(hex.equals("#004f89"))

		        {

		        	log.log(LogStatus.INFO, "Colour is blue which means the place is fixed for the organization use. So please book the other place.");

		        }
		        else if(hex.equals("#ffff00"))
		        {
		        	log.log(LogStatus.INFO, "Clour is Yellow which means it is available for booking. please start your booking.");
		        }
		        else
		        {
		        
		        log.log(LogStatus.FAIL, "Some Error Occured please Check your selected workplace colour ");
		        
		         
		        }
		        break;
		        }
		       
		       
		}
	    
		}
		catch (Exception e) {
			
			
			log.log(LogStatus.FAIL, e.getMessage());
		}
		
	}

	/*(26)*/
	//##############################################################################
	// ### Function Name  : WorkPlaceSelectorInFlexwhere
	// ### Created BY	  : Harsha Kumar -Mobinius Automation Testing Team 
	// ### Change History :
	// #############################################################################
	
	 /***
	 *  Description
	 *  This above method is used to verify the selected  Place after Booking in FlexWhere In WEB
	 *  if element is not available we will get Element Not Found Exception or element not able to click  .
	 * @throws AWTException 
	 * @throws Exception 
	 * @throws IOException 
	 * @throws HeadlessException 
	 *  
	 **/
	
	
	
	public static void WorkPlaceSelectorInFlexwhere(By elementQuery,String ClickingObject) {
		try {
			
				List<WebElement> elements = driver.findElements(elementQuery);
				Iterator<WebElement> i = elements.iterator();
			    while(i.hasNext()) {
			     WebElement element = i.next();
			     System.out.println(element.getAttribute("id") );
			     
				 if(element.getAttribute("id").equalsIgnoreCase(ClickingObject))
		        {
		        	
		        	System.out.println(element.getAttribute("id"));
			        String colourType =	element.getCssValue("fill");
			        System.out.println(element.getCssValue("fill"));
			        System.out.println("The Colour is "  + colourType);
			        String hex = Color.fromString(colourType).asHex();
			        System.out.println(hex);
			        
		        if(hex.equals("#66cc66")) 
		        {
		        	
		        	log.log(LogStatus.INFO, "Colour is green hence place is available for booking.");
		        	Thread.sleep(1000);
		        	element.click();
		        	Thread.sleep(1000);
		        	ClickButton(FlexWhereRegressionPage.JaButton, "Ja");
		        	Thread.sleep(2000);
		        			        	
		        }
		        else if (hex.equals("#cc6666")) 
		        {

		        	log.log(LogStatus.INFO, "Colour is Red which means your colleague is booked this place.Hence place is not available for booking at this time.");
		        }
		        else  if(hex.equals("#cfd8dc")) 

		        {

		        	log.log(LogStatus.INFO, "Colour is grey which means the place is disabled for booking . So please choose other place for book.");

		        }
		        else  if(hex.equals("#004f89"))

		        {

		        	log.log(LogStatus.INFO, "Colour is blue which means the place is fixed for the organization use. So please book the other place.");

		        }
		        else if(hex.equals("#ffff00"))
		        	
		        {
		        	log.log(LogStatus.INFO, "Clour is Yellow which means it is available for booking. please start your booking.");
		        }
		        else
		        	
		        {
		        
		        log.log(LogStatus.FAIL, "Some Error Occured please Check your selected workplace colour ");
		        
		         
		        }
		        break;
		        }
		       
		       
		}
	    
		}
		catch (Exception e) {
			
			
			log.log(LogStatus.FAIL, e.getMessage());
			System.out.println(e);
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	}